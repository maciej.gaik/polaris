import {
    AttributeIds,
    ClientMonitoredItemBase,
    ClientSession,
    ClientSubscription,
    coerceNodeId,
    DataValue,
    MonitoringMode,
    OPCUAClient,
    TimestampsToReturn,
    UserIdentityInfo,
    UserIdentityInfoUserName,
    UserTokenType,
    Variant,
    VariantArrayType,
    MessageSecurityMode,
    SecurityPolicy
} from 'node-opcua';

const connectionStrategy = {
    initialDelay: 1000,
    maxRetry: 1
  };

  const options = {
    applicationName: "MyClient",
    connectionStrategy: connectionStrategy,
    securityMode: MessageSecurityMode.None,
    securityPolicy: SecurityPolicy.None,
    endpoint_must_exist: false
  };
  const client = OPCUAClient.create(options);
  const endpointUrl = "opc.tcp://localhost:4334/ModuleTestServer";


  async function main() {
    try {
      // step 1 : connect to
      await client.connect(endpointUrl);
      console.log("Connected !");
  
      // step 2 : createSession
      const session = await client.createSession();
      console.log("Session created !");
  
      // step 3 : browse
      const browseResult = await session.browse("ns=1;i=1001");
      console.log("Browsing !");

      console.log("References of TestModule :");
      for (const reference of browseResult.references) {
        console.log("   -> ", reference.browseName.toString());
      }
  
      // step 4 : read a variable with readVariableValue
      const maxAge = 0;
      const nodeToRead = {
        nodeId: "ns=1;s=Service1.ProcessValueIn.VSclMax",
        attributeId: AttributeIds.Value
      };
      const dataValue = await session.read(nodeToRead, maxAge);
      console.log(" value ", dataValue.toString());
      // close session
      await session.close();
  
      // disconnecting
      await client.disconnect();
      console.log("done !");

    } catch(err) {
      console.log("An error has occured : ",err);
    }
  }
  
  main();